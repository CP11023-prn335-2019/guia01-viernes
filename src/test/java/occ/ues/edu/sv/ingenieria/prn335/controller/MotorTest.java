/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.controller;

import java.util.ArrayList;
import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author william
 */
public class MotorTest {
    
    public MotorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of mostrarSucursales method, of class Motor.
     */
//    @Test
//    public void testMostrarSucursales() {
//        System.out.println("mostrarSucursales");
//        Motor instance = new Motor();
//        ArrayList<Sucursal> expResult = null;
//        ArrayList<Sucursal> result = instance.mostrarSucursales();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of buscarActivos method, of class Motor.
     */
//    @Test
//    public void testBuscarActivos() {
//        System.out.println("buscarActivos");
//        String departamento = "";
//        Motor instance = new Motor();
//        ArrayList<Sucursal> expResult = null;
//        ArrayList<Sucursal> result = instance.buscarActivos(departamento);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of modificarSucursal method, of class Motor.
     */
@Test
    public void testModificarSucursal() {
        System.out.println("modificarSucursal");
        int id_sucursal = 7;
        String nombre = "Cinepolis La Union";
        String ciudad = "Puerto la union";
        String departamento = "La Union";
        String contacto = "Pedro Cuellar";
        boolean estado = true;
        
        Motor instance = new Motor();
        Boolean expResult = false;
        
        //para saber si se modifica
        for (int i = 0; i <instance.mostrarSucursales().size() ; i++) {
           if( (instance.mostrarSucursales().get(i).getId_sucursal()==id_sucursal) && (instance.mostrarSucursales().get(i).isEstado()==false) ){
               expResult=true;
           }
           
        }
        
        //resultado obtenido
        Boolean result = instance.modificarSucursal(id_sucursal, nombre, ciudad, departamento, contacto, estado);     
        System.out.println("expResult:"+expResult);
        System.out.println("result:"+result);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
