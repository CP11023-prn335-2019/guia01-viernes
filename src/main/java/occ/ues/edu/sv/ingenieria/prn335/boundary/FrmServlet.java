/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.boundary;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import occ.ues.edu.sv.ingenieria.prn335.controller.Motor;
import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author william
 */
public class FrmServlet extends HttpServlet {
    Motor motor=new Motor();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FrmServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FrmServlet at " + request.getContextPath() + "</h1>");
            
            //filtrar
            String btnFiltra=request.getParameter("btnFiltrar");
            String estado1;
            if(btnFiltra!=null){
            ArrayList<Sucursal> sucursal1=new ArrayList<Sucursal>();
//          int idSucursal=Integer.parseInt(request.getParameter("numIdSucursal"));
//          String nombre=request.getParameter("txtNombre");
//          String ciudad=request.getParameter("txtCiudad");
            String departamento=request.getParameter("txtDepartamento");
//          String contacto=request.getParameter("txtContacto");
//          boolean estado=Boolean.parseBoolean(request.getParameter("txtEstado"));
                try {
                    sucursal1=motor.buscarActivos(departamento);
                    out.println("<table border>");
                    out.println("<tr>");
                    out.println("<td><strong>"+"ID Sucursal"+"</strong></td>");
                    out.println("<td><strong>"+"Nombre"+"</strong></td>");
                    out.println("<td><strong>"+"Ciudad"+"</strong></td>");
                    out.println("<td><strong>"+"Departamento"+"</strong></td>");
                    out.println("<td><strong>"+"Contacto"+"</strong></td>");
                    out.println("<td><strong>"+"Estado"+"</strong></td>");
                    out.println("</tr>");
                            
                    if(sucursal1!=null){
                        for (int i = 0; i <sucursal1.size(); i++) {
                        out.println("<tr>");
                        out.println("<td>"+sucursal1.get(i).getId_sucursal()+"</td>");
                        out.println("<td>"+sucursal1.get(i).getNombre()+"</td>");
                        out.println("<td>"+sucursal1.get(i).getCiudad()+"</td>");
                        out.println("<td>"+sucursal1.get(i).getDepartamento()+"</td>");
                        out.println("<td>"+sucursal1.get(i).getContacto()+"</td>");
                       //cambiando la palabra true por activo y false por inactivo
                       
                            if(sucursal1.get(i).isEstado()){
                                estado1="Activo";
                            }else{
                                estado1="Inactivo";
                            }
                        out.println("<td>"+estado1+"</td>");
                        out.println("</tr>");
                      }
                    }
                    
                out.println("</table>"); 
                } catch (Exception e) {
                   System.err.println(""+e);
                }
            }
            
            //editar
            String btnEdita=request.getParameter("btnEditar");
            String estado2;
            
            if(btnEdita!=null){
            ArrayList<Sucursal> sucursal2=new ArrayList<Sucursal>();
            
            int idSucursal=Integer.parseInt(request.getParameter("numIdSucursal"));
            String nombre=request.getParameter("txtNombre");
            String ciudad=request.getParameter("txtCiudad");
            String departamento=request.getParameter("txtDepartamento");
            String contacto=request.getParameter("txtContacto");
            boolean estado=Boolean.parseBoolean(request.getParameter("txtEstado"));
            
                try {
                    if( motor.modificarSucursal(idSucursal, nombre, ciudad, departamento, contacto, estado) ){
                        sucursal2=motor.mostrarSucursales();
                        out.println("<table border>");
                        out.println("<tr>");
                        out.println("<td><strong>"+"ID Sucursal"+"</strong></td>");
                        out.println("<td><strong>"+"Nombre"+"</strong></td>");
                        out.println("<td><strong>"+"Ciudad"+"</strong></td>");
                        out.println("<td><strong>"+"Departamento"+"</strong></td>");
                        out.println("<td><strong>"+"Contacto"+"</strong></td>");
                        out.println("<td><strong>"+"Estado"+"</strong></td>");
                        out.println("</tr>");
                        if(sucursal2!=null){
                            for (int i = 0; i <sucursal2.size(); i++) {
                            out.println("<tr>");
                            out.println("<td>"+sucursal2.get(i).getId_sucursal()+"</td>");
                            out.println("<td>"+sucursal2.get(i).getNombre()+"</td>");
                            out.println("<td>"+sucursal2.get(i).getCiudad()+"</td>");
                            out.println("<td>"+sucursal2.get(i).getDepartamento()+"</td>");
                            out.println("<td>"+sucursal2.get(i).getContacto()+"</td>");
                            //cambiando la palabra true por activo y false por inactivo
                                if(sucursal2.get(i).isEstado()){
                                    estado2="Activo";
                                }else{
                                    estado2="Inactivo";
                                }
                            out.println("<td>"+estado2+"</td>");
                            out.println("</tr>");
                            }
                        }
                    
                    out.println("</table>"); 
                    }else{
                    out.println("<h1>"+"No se modifico no cumplio con los requisitos"+"</h1>");
                    }
                } catch (Exception e) {
                    System.err.println(""+e);
                }
                
            
            }
            
            
            
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
