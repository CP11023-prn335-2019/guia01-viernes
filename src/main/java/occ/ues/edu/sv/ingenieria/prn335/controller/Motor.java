/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//importacion estaba mala
package occ.ues.edu.sv.ingenieria.prn335.controller;

import java.util.ArrayList;



import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author Esperanza
 */
public class Motor {

    ArrayList<Sucursal> sucursales = new ArrayList<Sucursal>();
   
    public Motor() {
        sucursales.add(new Sucursal(1, "Cinepolis Metrocentro", "Santa Ana", "Santa Ana", "Juan Perez", true));
        sucursales.add(new Sucursal(2, "Cinepolis La Gran Via", "Antiguo Cuscatlan", "San Salvador", "Luis Lopez", true));
        sucursales.add(new Sucursal(3, "Cinepolis Metrocento SM", "San Miguel", "San Miguel", "Will Salgado", false));
        sucursales.add(new Sucursal(4, "Cinemark Metrocentro", "Soyapango", "San Salvador", "Encargado 1", false));
        sucursales.add(new Sucursal(5, "Cinepolis Metrocentro Sonso", "Sonsonate", "Sonsonate", "Encargado 2", true));
        sucursales.add(new Sucursal(6, "Cine La Union", "Puerto de la union", "La Union", "Encargado 3", true));
        sucursales.add(new Sucursal(7, "Cine La Union 2", "Ciudad de la union", "La Union", "Encargado 3", false));
    }
  
public ArrayList<Sucursal> mostrarSucursales() {return sucursales;}


//agregue buscarActivos
public ArrayList<Sucursal> buscarActivos(String departamento){
    ArrayList<Sucursal> sucursalesActivas=new ArrayList<Sucursal>();   
    
    for (int i = 0; i < sucursales.size(); i++) {
        if( (sucursales.get(i).getDepartamento().equalsIgnoreCase(departamento)) && (sucursales.get(i).isEstado()==true) && !(sucursales.get(i).getContacto().equalsIgnoreCase("Will Salgado") )  ){
        sucursalesActivas.add(sucursales.get(i));
        }    
    }
    

return sucursalesActivas;
}

//agregue modificar
public Boolean modificarSucursal(int id_sucursal, String nombre, String ciudad, String departamento, String contacto, boolean estado){
    boolean modificar=false;
    for (int i = 0; i < sucursales.size(); i++) {
        
        if( (sucursales.get(i).getId_sucursal()==id_sucursal) && (sucursales.get(i).isEstado()==false) ){
            sucursales.get(i).setId_sucursal(id_sucursal);
            sucursales.get(i).setNombre(nombre);
            sucursales.get(i).setCiudad(ciudad);
            sucursales.get(i).setDepartamento(departamento);
            sucursales.get(i).setContacto(contacto);
            sucursales.get(i).setEstado(estado);
            
            modificar=true;
        }
    }
    return modificar;
}



}

